/**
 *  \brief		Project 4: Hash Table
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		December 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "createListsOfInts.hpp"

#include "getRandInt.hpp"


ListsOfInts
createListsOfInts(const std::size_t NUM_OF_SEQUENCES, const std::size_t INCREMENTAL) {
	ListsOfInts listsOfInts(NUM_OF_SEQUENCES);

	for (std::size_t factor = 0; factor < NUM_OF_SEQUENCES; ++factor) {
		const std::size_t NUM_OF_ELEMENTS = INCREMENTAL * (factor + 1);

		listsOfInts[factor].reserve(NUM_OF_ELEMENTS);

		for (std::size_t i = 0; i < NUM_OF_ELEMENTS; ++i) {
			listsOfInts[factor].emplace_back(getRandInt());
		}
	}

	return listsOfInts;
}
