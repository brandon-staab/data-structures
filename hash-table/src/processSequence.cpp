/**
 *  \brief		Project 4: Hash Table
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		December 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "processSequence.hpp"

#include "Global.hpp"

#include <stdexcept>


namespace {
	using namespace Global;


	void
	linearProbing(const int key, std::size_t& index, const bool* in_use, std::size_t& collisons) {
		std::size_t loop_counter = 0;
		index = key % TABLE_SIZE;

		while (in_use[index]) {
			if (++loop_counter == TABLE_SIZE) {
				throw std::runtime_error("Hash table is too full.");
			}

			index = (index + 1) % TABLE_SIZE;
		}


		collisons += loop_counter;
	}


	void
	quadraticProbing(const int key, std::size_t& index, const bool* in_use, std::size_t& collisons) {
		std::size_t loop_counter = 0;

		index = key % TABLE_SIZE;

		while (in_use[index]) {
			if (++loop_counter == TABLE_SIZE) {
				throw std::runtime_error("Hash table is too full.");
			}

			if (loop_counter % 2 == 0) {
				index = (key - loop_counter * loop_counter) % TABLE_SIZE;
			} else {
				index = (key + loop_counter * loop_counter) % TABLE_SIZE;
			}
		}

		collisons += loop_counter;
	}


	void
	doubleHashing(const int key, std::size_t& index, const bool* in_use, std::size_t& collisons) {
		std::size_t loop_counter = 0;

		auto h1 = [](int k) { return k % TABLE_SIZE; };
		auto h2 = [](int k) { return (k % 7) + 7; };
		auto hash = [=](int i, int k) { return h1(k) + i * h2(k); };

		index = hash(loop_counter, key) % TABLE_SIZE;
		while (in_use[index]) {
			if (++loop_counter == TABLE_SIZE) {
				throw std::runtime_error("Hash table is too full.");
			}

			index = hash(loop_counter, key) % TABLE_SIZE;
		}

		collisons += loop_counter;
	}


	std::size_t
	makeHashTable(const ListOfInts& list, void (*method)(const int, std::size_t&, const bool*, std::size_t&)) {
		std::size_t collisons = 0;
		int table[TABLE_SIZE]{0};
		bool in_use[TABLE_SIZE]{false};
		std::size_t index = 0;

		for (auto& num : list) {
			method(num, index, in_use, collisons);

			table[index] = num;
			in_use[index] = true;
		}

		return collisons;
	}
}  // namespace


bool
processSequence(const ListOfInts& list, Result& result) {
	bool no_exception = true;
	std::size_t collisons[COLLISONMETHOD_MAX + 1]{0};
	result.seq_size = list.size();

	try {
		collisons[LinearProbing] = makeHashTable(list, linearProbing);
	} catch (std::runtime_error& re) {
		no_exception = false;
		++result.exceptions[LinearProbing];
	}

	try {
		collisons[QuadraticProbing] = makeHashTable(list, quadraticProbing);
	} catch (std::runtime_error& re) {
		no_exception = false;
		++result.exceptions[QuadraticProbing];
	}

	try {
		collisons[DoubleHashing] = makeHashTable(list, doubleHashing);
	} catch (std::runtime_error& re) {
		no_exception = false;
		++result.exceptions[DoubleHashing];
	}

	if (no_exception) {
		result.collisons[LinearProbing] += collisons[LinearProbing];
		result.collisons[QuadraticProbing] += collisons[QuadraticProbing];
		result.collisons[DoubleHashing] += collisons[DoubleHashing];
	}

	return no_exception;
}
