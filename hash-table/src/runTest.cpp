/**
 *  \brief		Project 4: Hash Table
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		December 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "runTest.hpp"

#include "createListsOfInts.hpp"
#include "processSequence.hpp"


using namespace Global;

constexpr bool ERROR = false;


Results
runTest(const std::size_t samples) {
	std::size_t current_sample = 0;
	ListsOfInts listsOfInts;
	Results results(NUM_OF_SEQUENCES);
	Results temp_results(NUM_OF_SEQUENCES);

	while (++current_sample <= samples) {
	start_test:
		listsOfInts = createListsOfInts(NUM_OF_SEQUENCES);
		for (auto& result : temp_results) {
			for (std::size_t i = 0; i <= COLLISONMETHOD_MAX; ++i) {
				result.collisons[i] = 0;
			}
		}

		for (std::size_t i = 0; i < listsOfInts.size(); ++i) {
			if (ERROR == processSequence(listsOfInts[i], temp_results[i])) {
				goto start_test;
			}
		}

		for (std::size_t i = 0; i < NUM_OF_SEQUENCES; ++i) {
			results[i].update(temp_results[i]);
		}
	}

	for (auto& result : results) {
		for (std::size_t method = 0; method <= COLLISONMETHOD_MAX; ++method) {
			result.collisons[method] /= samples;
			result.exceptions[method] /= samples;
		}
	}

	return results;
}
