/**
 *  \brief		Project 4: Hash Table
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		December 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "printResults.hpp"

#include <iomanip>
#include <iostream>


const int MAX_ROW_TITLE_LEN = 42;
const int NUM_LEN = 8;

void
printResults(const Results& results) {
	std::cout << '\n' << std::left << std::setw(MAX_ROW_TITLE_LEN) << "Sequence Size:";
	for (auto i : results) {
		std::cout << std::setw(NUM_LEN) << i.seq_size;
	}
	std::cout << '\n';

	std::cout << std::setw(MAX_ROW_TITLE_LEN) << "Collisions using Linear Probing:";
	for (auto result : results) {
		std::cout << std::setw(NUM_LEN) << result.collisons[LinearProbing];
	}
	std::cout << '\n';

	std::cout << std::setw(MAX_ROW_TITLE_LEN) << "Collisions using Quadratic Probing:";
	for (auto result : results) {
		std::cout << std::setw(NUM_LEN) << result.collisons[QuadraticProbing];
	}
	std::cout << '\n';

	std::cout << std::setw(MAX_ROW_TITLE_LEN) << "Collisons using Double Hashing:";
	for (auto result : results) {
		std::cout << std::setw(NUM_LEN) << result.collisons[DoubleHashing];
	}
	std::cout << "\n\n";

	std::cout << std::setw(MAX_ROW_TITLE_LEN) << "Number of failures for Quadratic hashing:";
	for (auto result : results) {
		std::cout << std::setw(NUM_LEN) << result.exceptions[QuadraticProbing];
	}
	std::cout << '\n';

	std::cout << std::setw(MAX_ROW_TITLE_LEN) << "Number of failures for Double hashing";
	for (auto result : results) {
		std::cout << std::setw(NUM_LEN) << result.exceptions[DoubleHashing];
	}
	std::cout << '\n';
}
