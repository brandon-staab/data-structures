/**
 *  \brief		Project 4: Hash Table
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		December 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Results.hpp"


void
Result::update(const Result& that) {
	for (std::size_t i = 0; i <= COLLISONMETHOD_MAX; ++i) {
		seq_size = that.seq_size;
		collisons[i] += that.collisons[i];
		exceptions[i] += that.exceptions[i];
	}
}
