/**
 *  \brief		Project 4: Hash Table
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		December 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Global.hpp"

namespace Global {
	const std::size_t TABLE_SIZE = 10001;
	const std::size_t NUM_OF_SEQUENCES = 10;
	const std::size_t INCREMENTAL_SIZE = 1000;
	const std::size_t SAMPLES = 1;
}  // namespace Global
