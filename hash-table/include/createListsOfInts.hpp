/**
 *  \brief		Project 4: Hash Table
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		December 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once


#include "Global.hpp"
#include "ListsOfInts.hpp"


ListsOfInts createListsOfInts(const std::size_t NUM_OF_SEQUENCES = Global::NUM_OF_SEQUENCES, const std::size_t INCREMENTAL = Global::INCREMENTAL_SIZE);
