/**
 *  \brief		Project 4: Hash Table
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		December 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include <cstddef>


namespace Global {
	extern const std::size_t TABLE_SIZE;
	extern const std::size_t NUM_OF_SEQUENCES;
	extern const std::size_t INCREMENTAL_SIZE;
	extern const std::size_t SAMPLES;
}  // namespace Global
