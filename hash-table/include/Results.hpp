/**
 *  \brief		Project 4: Hash Table
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		December 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */
#pragma once

#include <vector>


enum CollisonMethod {
	LinearProbing,
	QuadraticProbing,
	DoubleHashing,

	COLLISONMETHOD_MAX = DoubleHashing
};

struct Result {
	std::size_t seq_size{0};
	std::size_t collisons[COLLISONMETHOD_MAX + 1]{0};
	std::size_t exceptions[COLLISONMETHOD_MAX + 1]{0};

	void update(const Result& that);
};

using Results = std::vector<Result>;
