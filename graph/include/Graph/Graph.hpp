/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include <forward_list>
#include <unordered_map>
#include <unordered_set>
#include <utility>


struct NotDAG : std::domain_error {
	NotDAG() : std::domain_error("Graph not a DAG.") {}
};


using vertex_hash = std::size_t;


template <typename T>
class Graph {
  public:
	void insert(T const& from, T const& to, double const weight);

	void addVertex(T const& val);
	void addVertex(T&& val);
	typename std::unordered_set<T>::const_iterator getVertex(T const& val) const;
	T const& getVertex(vertex_hash const hash) const;
	bool hasVertex(T const& val) const;

	void addEdge(T const& from, T const& to, double const weight);
	std::pair<typename std::unordered_multimap<vertex_hash, double>::const_iterator, typename std::unordered_multimap<vertex_hash, double>::const_iterator> getEdges(
		T const& from,
		T const& to) const;
	bool hasEdge(T const& from, T const& to, double const weight) const;
	std::pair<double, double> getMinMaxEdge(T const& from, T const& to) const;
	std::pair<typename std::unordered_multimap<vertex_hash, double>::const_iterator, typename std::unordered_multimap<vertex_hash, double>::const_iterator> getOutEdges(
		vertex_hash const hash) const;

	void clear();

	typename std::unordered_map<vertex_hash, T>::const_iterator begin() const;
	typename std::unordered_map<vertex_hash, T>::const_iterator end() const;

	vertex_hash hash(const T& val) const;

  private:
	std::unordered_map<vertex_hash, T> vertexs;
	std::unordered_map<vertex_hash, std::unordered_multimap<vertex_hash, double>> edges;
};


#include "Graph/Graph.tpp"


enum Mark {
	Unmarked,
	Temporary,
	Permanent,

	MARK_MAX = Permanent
};

#include "getDegrees.hpp"
#include "getZeroInDegree.hpp"
#include "hasCycle.hpp"
#include "topologicalSort.hpp"
