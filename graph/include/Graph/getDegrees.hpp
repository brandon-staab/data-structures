/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once


using degree_map = std::unordered_map<vertex_hash, std::size_t>;

template <typename T>
std::pair<degree_map, degree_map> getDegrees(Graph<T> const& graph);

#include "Graph/getDegrees.tpp"
