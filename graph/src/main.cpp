/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Graph/Graph.hpp"
#include "getConsoleInput.hpp"
#include "loadGraph.hpp"
#include "menu.hpp"

#include <string>


int
main() {
	Graph<int> graph;

	bool running_flag = true;

	do {
		std::cout << '\n';
		auto choice = menu();

		switch (choice) {
			case 1: {
				std::cout << "Enter file name to open.\n";

			getFile:
				auto file_name = getConsoleInput<std::string>();

				if (file_name.empty()) {
					std::cout << "File name can not be empty.\n";
					goto getFile;
				}

				loadGraph(file_name.data(), graph);
			} break;
			case 2: {
				try {
					auto const list = topologicalSort(graph);
					std::cout << "Topological Sort: ";
					for (auto const i : list) {
						std::cout << graph.getVertex(i) << ' ';
					}
				} catch (std::exception const& e) {
					std::cerr << "Exception Caught: " << e.what();
				}
				std::cout << '\n';
			} break;
			case 3: {
				auto list = hasCycle(graph);
				if (list.empty()) {
					std::cout << "No Cycle\n";
				} else {
					std::cout << "Cycle found: ";
					for (auto const i : list) {
						std::cout << graph.getVertex(i) << ' ';
					}
					std::cout << '\n';
				}
			} break;
			case 4: {
				running_flag = false;
			} break;
		}
	} while (running_flag);

	return 0;
}
