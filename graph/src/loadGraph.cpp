/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "loadGraph.hpp"

#include <fstream>
#include <sstream>
#include <string>


const std::string MSG_ERR_OPENFILE = "Unable to open file.\n";

/*** Explicit instantiation definition  ***************************************/
template void loadGraph<int>(char const* file_name, Graph<int>& graph);
template void loadGraph<std::string>(char const* file_name, Graph<std::string>& graph);


template <typename T>
void
loadGraph(char const* file_name, Graph<T>& graph) {
	std::ifstream file(file_name);
	std::string line;

	T from;
	T to;
	double weight;


	if (!file.is_open()) {
		std::cerr << MSG_ERR_OPENFILE;
		return;
	}

	graph.clear();
	while (std::getline(file, line)) {
		if (!line.empty()) {
			if ((static_cast<std::stringstream>(line) >> from >> to >> weight).fail()) {
				std::cerr << "Could not add \"" << line << "\" to the graph.\n";
			} else {
				graph.insert(from, to, weight);
			}
		}
	}
}
