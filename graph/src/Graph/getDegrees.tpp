/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include <unordered_map>


template <typename T>
std::pair<degree_map, degree_map>
getDegrees(Graph<T> const& graph) {
	std::pair<degree_map, degree_map> degree;

	for (auto& vert_from : graph) {
		if (degree.first.count(vert_from.first) == 0) {
			degree.first[vert_from.first] = 0;
		}

		if (degree.second.count(vert_from.first) == 0) {
			degree.second[vert_from.first] = 0;
		}

		auto const range = graph.getOutEdges(vert_from.first);
		for (auto it_vert_to = range.first; it_vert_to != range.second; ++it_vert_to) {
			auto& vert_to = *it_vert_to;

			++(degree.first)[vert_to.first];
			++(degree.second)[vert_from.first];
		}
	}

	return degree;
}
