/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include <stack>


template <typename T>
void
visitCycle(vertex_hash const vertex,
		   Graph<T> const& graph,
		   std::forward_list<vertex_hash>& list,
		   std::unordered_map<vertex_hash, Mark>& visited,
		   std::stack<vertex_hash>& visitStack) {
	visitStack.push(vertex);
	if (visited[vertex] == Permanent) {
		return;
	} else if (visited[vertex] == Temporary) {
		throw NotDAG();
	}

	visited[vertex] = Temporary;
	auto const range = graph.getOutEdges(vertex);
	for (auto it = range.first; it != range.second; ++it) {
		visitCycle(it->first, graph, list, visited, visitStack);
		visitStack.pop();
	}
	visited[vertex] = Permanent;
	list.push_front(vertex);
}


template <typename T>
std::forward_list<vertex_hash>
hasCycle(Graph<T> const& graph) {
	std::forward_list<vertex_hash> list;
	std::stack<vertex_hash> visitStack;
	std::queue<vertex_hash> start_nodes = getZeroInDegree(graph);
	std::unordered_map<vertex_hash, Mark> visited;

	try {
		while (!start_nodes.empty()) {
			visitCycle(start_nodes.front(), graph, list, visited, visitStack);
			visitStack.pop();
			start_nodes.pop();
		}
	} catch (NotDAG& e) {
	}

	std::forward_list<vertex_hash> cycle;
	while (!visitStack.empty()) {
		cycle.push_front(visitStack.top());
		visitStack.pop();
	}

	return cycle;
}
