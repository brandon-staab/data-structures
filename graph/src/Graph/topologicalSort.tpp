/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include <queue>


template <typename T>
void
visitTopo(vertex_hash const vertex, Graph<T> const& graph, std::forward_list<vertex_hash>& list, std::unordered_map<vertex_hash, Mark>& visited) {
	if (visited[vertex] == Permanent) {
		return;
	} else if (visited[vertex] == Temporary) {
		throw NotDAG();
	}

	visited[vertex] = Temporary;
	auto const range = graph.getOutEdges(vertex);

	auto cmp_edge_less = [](std::pair<vertex_hash, double> const& lhs, std::pair<vertex_hash, double> const& rhs) { return lhs.second < rhs.second; };


	std::priority_queue<std::pair<vertex_hash, double>, std::vector<std::pair<vertex_hash, double>>, decltype(cmp_edge_less)> pQueue(range.first, range.second, cmp_edge_less);
	while (!pQueue.empty()) {
		visitTopo(pQueue.top().first, graph, list, visited);
		pQueue.pop();
	}
	visited[vertex] = Permanent;
	list.push_front(vertex);
}


template <typename T>
std::forward_list<vertex_hash>
topologicalSort(Graph<T> const& graph) {
	std::forward_list<vertex_hash> list;
	std::queue<vertex_hash> start_nodes = getZeroInDegree(graph);

	std::unordered_map<vertex_hash, Mark> visited;
	while (!start_nodes.empty()) {
		visitTopo(start_nodes.front(), graph, list, visited);
		start_nodes.pop();
	}

	return list;
}
