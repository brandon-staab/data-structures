/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

template <typename T>
std::queue<vertex_hash>
getZeroInDegree(Graph<T> const& graph) {
	std::queue<vertex_hash> vertexes;

	for (std::pair<vertex_hash, vertex_hash> vertex : getDegrees(graph).first) {
		if (vertex.second == 0) {
			vertexes.push(vertex.first);
		}
	}

	return vertexes;
}
