/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include <algorithm>
#include <cassert>
#include <iostream>


template <typename T>
void
Graph<T>::insert(T const& from, T const& to, double const weight) {
	if (!hasVertex(from)) {
		addVertex(from);
	}

	if (!hasVertex(to)) {
		addVertex(to);
	}

	if (!hasEdge(from, to, weight)) {
		addEdge(from, to, weight);
	}
}


template <typename T>
void
Graph<T>::addVertex(T const& val) {
	if (vertexs.insert(std::make_pair(hash(val), val)).second) {
		edges.emplace(hash(val), std::unordered_multimap<vertex_hash, double>());
	} else {
		throw std::invalid_argument("Vertex already exists");
	}
}


template <typename T>
void
Graph<T>::addVertex(T&& val) {
	if (vertexs.insert(std::make_pair(hash(val), val)).second) {
		edges.emplace(hash(val), std::unordered_multimap<vertex_hash, double>());
	} else {
		throw std::invalid_argument("Vertex already exists");
	}
}


template <typename T>
typename std::unordered_set<T>::const_iterator
Graph<T>::getVertex(T const& val) const {
	return vertexs.find(hash(val));
}


template <typename T>
const T&
Graph<T>::getVertex(const vertex_hash hash) const {
	return vertexs.equal_range(hash).first->second;
}

template <typename T>
bool
Graph<T>::hasVertex(T const& val) const {
	return vertexs.find(hash(val)) != end();
}


template <typename T>
void
Graph<T>::addEdge(T const& from, T const& to, double const weight) {
	edges[hash(from)].emplace(hash(to), weight);
}


template <typename T>
std::pair<typename std::unordered_multimap<vertex_hash, double>::const_iterator, typename std::unordered_multimap<vertex_hash, double>::const_iterator>
Graph<T>::getEdges(T const& from, T const& to) const {
	return edges.at(hash(from)).equal_range(hash(to));
}


template <typename T>
bool
Graph<T>::hasEdge(T const& from, T const& to, double const weight) const {
	auto range = getEdges(from, to);

	for (auto it = range.first; it != range.second; ++it) {
		if (weight == it->second) return true;
	}

	return false;
}


template <typename T>
std::pair<double, double>
Graph<T>::getMinMaxEdge(T const& from, T const& to) const {
	const auto range = getEdges(from, to);

	double min = range.first->second;
	double max = range.first->second;

	for (auto it = range.first; it != range.second; ++it) {
		min = std::min(min, it->second);
		max = std::max(max, it->second);
	}

	return std::make_pair(min, max);
}


template <typename T>
std::pair<typename std::unordered_multimap<vertex_hash, double>::const_iterator, typename std::unordered_multimap<vertex_hash, double>::const_iterator>
Graph<T>::getOutEdges(vertex_hash const hash) const {
	return std::make_pair(edges.at(hash).begin(), edges.at(hash).end());
}


template <typename T>
void
Graph<T>::clear() {
	vertexs.clear();
	edges.clear();
}


template <typename T>
typename std::unordered_map<vertex_hash, T>::const_iterator
Graph<T>::begin() const {
	return vertexs.begin();
}


template <typename T>
typename std::unordered_map<vertex_hash, T>::const_iterator
Graph<T>::end() const {
	return vertexs.end();
}


template <typename T>
vertex_hash
Graph<T>::hash(T const& val) const {
	static std::hash<T> fn;

	return fn(val);
}
