/**
 *  \brief		Project 3: Graph
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		November 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "menu.hpp"

#include "getConsoleInput.hpp"

#include <iostream>


int
menu() {
	std::cout << "(1) Open input file\n(2) Topological sort\n(3) Detect Cycle\n(4) Quit\n";

getInput:
	auto selection = getConsoleInput<int>();

	if (selection < 1 || selection > 4) {
		std::cout << "Valid options are between 1 and 4.\n";
		goto getInput;
	}

	return selection;
}
