/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "ListStack.hpp"


const bool
isBinaryOp(char const c) {
	return (c == '+') || (c == '-') || (c == '*') || (c == '/');
}


template <class T>
T
evaluatePostfix(Postfix<T>& expr) {
	ListStack<T> numStack{};

	// We read the tokens in one at a time.
	while (!expr.output.empty()) {
		/* If it is a binary operator, pop the top two elements from the stack,
		 * apply the operator, and pushthe result back on the stack. */
		if (isBinaryOp(expr.output.front())) {
			T nums[2];
			nums[0] = numStack.top();
			numStack.pop();
			nums[1] = numStack.top();
			numStack.pop();
			switch (expr.output.front()) {
				case '+':
					numStack.push(nums[1] + nums[0]);
					break;
				case '-':
					numStack.push(nums[1] - nums[0]);
					break;
				case '*':
					numStack.push(nums[1] * nums[0]);
					break;
				case '/':
					if (nums[0] == 0) {
						throw std::runtime_error("division by zero");
					}
					numStack.push(nums[1] / nums[0]);
					break;
			}
		} else {  // If it is an integer, push it on the stack
			expr.tokens.setBegin();
			do {
				if (expr.tokens.getItem().name == expr.output.front()) {
					numStack.push(expr.tokens.getItem().value);
				}

				expr.tokens.next();
			} while (!expr.tokens.isEnd());
		}

		expr.output.pop_front();
	}

	return numStack.top();
}
