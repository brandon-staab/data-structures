/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "menu.hpp"

#include "getConsoleInput.hpp"

#include <iostream>


int
menu() {
	std::cout << R"(
(1) Convert Infix to Postfix
(2) Evaluate Expression
(3) Exit
)";

getInput:
	int selection = getConsoleInput<int>();

	if (selection < 1 || selection > 3) {
		std::cout << "Valid options are between 1 and 3.\n";
		goto getInput;
	}


	return selection;
}
