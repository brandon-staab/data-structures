/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "ListStack.hpp"


inline const std::size_t
getPrecedence(char const op) {
	switch (op) {
		case '+':
		case '-':
			return 1;
			break;
		case '*':
		case '/':
			return 2;
			break;
		default:
			return 10;
			break;
	}
}


template <typename T>
inline void
writeToOutput(Postfix<T>& expr, std::string const cToken, char tokenSymbol) {
	/* NOTE: stod allows for floats to be read in, but could result in
	 * narrowing conversion.  We can safely ignore this because our
	 * input is guaranteed to be perfect. */
	expr.tokens.push_back(Token<T>{tokenSymbol, static_cast<T>(std::stod(cToken))});
	expr.output.push_back(tokenSymbol);
}


template <>
inline void
writeToOutput(Postfix<char>& expr, std::string const cToken, char tokenSymbol) {
	if (cToken.length() > 1) {
		throw std::invalid_argument("operands can only be one character long");
	}

	if (!isalpha(cToken.front())) {
		throw std::invalid_argument("operands must be a letter");
	}

	expr.tokens.push_back(Token<char>{tokenSymbol, cToken.front()});
	expr.output.push_back(tokenSymbol);
}


template <typename T>
Postfix<T>
convertInfixToPostfix(std::string const& input) {
	auto pos = 0;
	auto cToken = std::string("");
	auto tokenSymbol = 'A';
	auto operators = ListStack<char>();
	auto expr = Postfix<T>();

	if (input.back() != '#') {
		throw std::invalid_argument("infix must end with a #");
	}

	if (input.length() == 1) {
		throw std::invalid_argument("no expression given");
	}


	// Read tokens
	do {
		// Reads until space and create the current working token
		do {
			if (!isspace(input[pos])) cToken.push_back(input[pos]);
		} while (isspace(input[++pos]) == 0);

		// Token is operator or numeric value
		if ((cToken.size() == 1) &&
			((cToken.front() == '+') || (cToken.front() == '-') || (cToken.front() == '*') || (cToken.front() == '/') || (cToken.front() == '(') || (cToken.front() == ')'))) {
			if (operators.empty()) {
				/** If a token is an operator, push it to the stack, if the
				 * stack is empty. */
				operators.push(cToken.front());
			} else {
				if (cToken.front() == ')') {
					/* If a token is a right parentheses ')', you pop entries
					 * until you meet '('. */
					while (!operators.empty() && (operators.top() != '(')) {
						expr.output.push_back(operators.top());
						operators.pop();
					}
					operators.pop();
				} else if (cToken.front() == '(') {
					/* If a token is a left parentheses '(', push it to the
					 * stack */
					operators.push(cToken.front());
				} else {
					/* If the stack is not empty, you pop entries with higher
					 * or equal priority and only then you push that token to
					 * the stack. */
					while (!operators.empty() && (getPrecedence(operators.top()) >= getPrecedence(cToken.front())) && (operators.top() != '(')) {
						expr.output.push_back(operators.top());
						operators.pop();
					}
					operators.push(cToken.front());
				}
			}
		} else {  // If a token is an integer or operand, write it into the output
			writeToOutput<T>(expr, cToken, tokenSymbol++);
		}

		cToken.clear();
	} while (input[++pos] != '#');

	/* When you finish reading the string, you pop up all tokens which are left
	 * there. */
	while (!operators.empty()) {
		expr.output.push_back(operators.top());
		operators.pop();
	}

	return expr;
}
