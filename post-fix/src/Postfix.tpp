/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include <cctype>
#include <iostream>


template <typename T>
std::ostream&
operator<<(std::ostream& os, Postfix<T> const& expr) {
	expr.output.setBegin();
	do {
		if (isalpha(expr.output.getItem())) {
			expr.tokens.setBegin();
			do {
				if (expr.tokens.getItem().name == expr.output.getItem()) {
					os << expr.tokens.getItem().value << ' ';
				}

				expr.tokens.next();
			} while (!expr.tokens.isEnd());
		} else {
			os << expr.output.getItem() << ' ';
		}

		expr.output.next();
	} while (!expr.output.isEnd());

	return os;
}
