/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "askSub.hpp"
#include "convertInfixToPostfix.hpp"
#include "evaluatePostfix.hpp"
#include "getConsoleInput.hpp"
#include "menu.hpp"

#include <stdexcept>


using numeric_type = int;


int
main() {
	auto runningFlag = true;
	std::cout << "Infix expressions have their tokens separated by a space and are terminated by a \'#\'.\n";

	while (runningFlag) {
		try {
			switch (menu()) {
				case 1: {
					std::cout << "Enter an infix expression:\n";
					auto const input = getConsoleInput<std::string>();
					std::cout << "\nInfix Expression: " << input.substr(0, input.length() - 1) << '\n';
					std::cout << "Postfix Expression: " << convertInfixToPostfix<char>(input) << '\n';
				} break;
				case 2: {
					std::cout << "Enter an infix expression:\n";
					auto const post = convertInfixToPostfix<char>(getConsoleInput<std::string>());

					std::cout << '\n';
					auto expr = askSub<numeric_type>(post);
					std::cout << "\nAnswer: " << evaluatePostfix<numeric_type>(expr) << '\n';
				} break;
				case 3:
					runningFlag = false;
					break;
			}
		} catch (std::exception const& ex) {
			std::cerr << "Exception caught: " << ex.what() << '\n';
		}
	}
}
