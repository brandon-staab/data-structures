/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "askSub.hpp"

#include "getConsoleInput.hpp"


template <typename T>
Postfix<T>
askSub(Postfix<char> const& postfix) {
	bool subbed[256]{false};
	T substitution[256];
	char tokenSymbol{'A'};
	Postfix<T> ret_postfix;


	postfix.output.setBegin();
	while (!postfix.output.isEnd()) {
		postfix.tokens.setBegin();
		while (!postfix.tokens.isEnd()) {
			auto const var = postfix.tokens.getItem().value;
			auto const val = static_cast<int>(var);

			if (isalpha(postfix.output.getItem()) && !subbed[val]) {
				std::cout << "Enter a value for \'" << var << "\'\n";

				substitution[val] = getConsoleInput<T>();
				subbed[val] = true;
			}

			postfix.tokens.next();
		}

		postfix.output.next();
	}

	postfix.output.setBegin();
	while (!postfix.output.isEnd()) {
		auto& out = postfix.output.getItem();
		auto& token = postfix.tokens.getItem();
		auto const val = static_cast<int>(token.value);

		if (isalpha(out)) {
			postfix.tokens.setBegin();
			while (!postfix.tokens.isEnd()) {
				if (token.name == out) {
					auto new_token = Token<T>{tokenSymbol, substitution[val]};
					ret_postfix.tokens.push_back(new_token);
					ret_postfix.output.push_back(tokenSymbol++);
				}

				postfix.tokens.next();
			}
		} else {
			ret_postfix.output.push_back(out);
		}

		postfix.output.next();
	}


	return ret_postfix;
}
