/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */


/*** Member functions *****************************************************/
template <typename T>
inline bool
ListStack<T>::empty() const {
	return stack.empty();
}


template <typename T>
inline int
ListStack<T>::size() const {
	return stack.size();
}


template <typename T>
inline typename ListStack<T>::reference
ListStack<T>::top() {
	return stack.back();
}


template <typename T>
inline typename ListStack<T>::const_reference
ListStack<T>::top() const {
	return const_cast<typename ListStack<T>::const_reference>(stack.back());
}


template <typename T>
inline void
ListStack<T>::push(typename ListStack<T>::const_reference val) {
	stack.push_back(val);
}


template <typename T>
inline void
ListStack<T>::push(value_type&& val) {
	stack.push_back(val);
}


template <typename T>
inline void
ListStack<T>::pop() {
	stack.pop_back();
}
