/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include <algorithm>
#include <cassert>
#include <iostream>


template <typename T>
ArrayList<T>::ArrayList(size_type size) : frontPos(NULL_POS), backPos(NULL_POS + 1), cursor(NULL_POS), arraySize(size), array(new typename ArrayList<T>::value_type[size]) {}


template <typename T>
ArrayList<T>::ArrayList(typename ArrayList<T>::const_reference other) :
	frontPos(other.frontPos),
	backPos(other.backPos),
	cursor(other.cursor),
	arraySize(other.size),
	array(new typename ArrayList<T>::value_type[other.size]) {
	std::copy(other.array, other.array + other.size, array);
}


template <typename T>
void
ArrayList<T>::operator=(typename ArrayList<T>::reference other) {
	frontPos = other.frontPos;
	backPos = other.backPos;
	cursor = other.cursor;
	arraySize = other.arraySize;
	array = other.array;

	other.frontPos = other.backPos = NULL_POS;
	other.arraySize = 0;
	other.array = nullptr;
}


/*** Capacity *****************************************************************/
template <typename T>
bool
ArrayList<T>::empty() const noexcept {
	return size() == 0;
}


template <typename T>
typename ArrayList<T>::size_type
ArrayList<T>::size() const noexcept {
	return ((backPos - frontPos) % arraySize) - 1;
}


template <typename T>
typename ArrayList<T>::size_type
ArrayList<T>::max_size() const noexcept {
	return arraySize;
}


/*** Element access ***********************************************************/
template <typename T>
typename ArrayList<T>::reference
ArrayList<T>::front() {
	auto const idx = (frontPos + 1) % arraySize;

	return array[idx];
}


template <typename T>
typename ArrayList<T>::const_reference
ArrayList<T>::front() const {
	auto const idx = (frontPos + 1) % arraySize;

	return array[idx];
}


template <typename T>
typename ArrayList<T>::reference
ArrayList<T>::back() {
	auto const idx = (backPos - 1) % arraySize;

	return array[idx];
}


template <typename T>
typename ArrayList<T>::const_reference
ArrayList<T>::back() const {
	auto const idx = (backPos - 1) % arraySize;

	return array[idx];
}


/*** Modifiers ****************************************************************/
/*** Front ***/
template <typename T>
void
ArrayList<T>::push_front(typename ArrayList<T>::const_reference val) {
	array[frontPos] = val;
	if (addFrontPos()) {
		array[frontPos] = val;
		addFrontPos();
	}
}


template <typename T>
void
ArrayList<T>::push_front(value_type&& val) {
	array[frontPos] = val;
	if (addFrontPos()) {
		array[frontPos] = val;
		addFrontPos();
	}
}


template <typename T>
void
ArrayList<T>::pop_front() {
	delFrontPos();
}


/*** Back ***/
template <typename T>
void
ArrayList<T>::push_back(const_reference val) {
	array[backPos] = val;
	if (addBackPos()) {
		array[backPos] = val;
		addBackPos();
	}
}


template <typename T>
void
ArrayList<T>::push_back(value_type&& val) {
	array[backPos] = val;
	if (addBackPos()) {
		array[backPos] = val;
		addBackPos();
	}
}


template <typename T>
void
ArrayList<T>::pop_back() {
	delBackPos();
}


/*** Utility ***/
template <typename T>
void
ArrayList<T>::resize(size_type n) {
	assert(n > size());
	std::unique_ptr<ArrayList<T>::value_type[]> newArray(new T[n]);

	setBegin();
	int i = 1;
	while (!isEnd()) {
		newArray[i] = getItem();
		next();
		++i;
	}

	frontPos = 0;
	backPos = i;
	arraySize = n;

	array = std::move(newArray);
}


/*** Cursor *******************************************************************/
template <typename T>
void
ArrayList<T>::setBegin() const {
	cursor = (frontPos + 1) % arraySize;
}


template <typename T>
void
ArrayList<T>::setEnd() const {
	cursor = (backPos - 1) % arraySize;
}


template <typename T>
bool
ArrayList<T>::isBegin() const {
	return cursor == frontPos;
}


template <typename T>
bool
ArrayList<T>::isEnd() const {
	return cursor == backPos;
}


template <typename T>
void
ArrayList<T>::next() const {
	cursor = (cursor + 1) % arraySize;
}


template <typename T>
void
ArrayList<T>::prev() const {
	cursor = (cursor - 1) % arraySize;
}


template <typename T>
typename ArrayList<T>::reference
ArrayList<T>::getItem() {
	return array[cursor];
}


template <typename T>
typename ArrayList<T>::const_reference
ArrayList<T>::getItem() const {
	return array[cursor];
}


/*** Private Utility Members **************************************************/
template <typename T>
bool
ArrayList<T>::addFrontPos() {
	typename ArrayList<T>::pos_type oldFrontPos = frontPos;
	frontPos = (frontPos - 1) % arraySize;
	if (needsResized()) {
		frontPos = oldFrontPos;
		resize(2 * arraySize);
		return true;
	}

	return false;
}


template <typename T>
bool
ArrayList<T>::addBackPos() {
	typename ArrayList<T>::pos_type oldBackPos = backPos;
	backPos = (backPos + 1) % arraySize;
	if (needsResized()) {
		backPos = oldBackPos;
		resize(2 * arraySize);
		return true;
	}

	return false;
}


template <typename T>
void
ArrayList<T>::delFrontPos() {
	if (!empty()) {
		frontPos = (frontPos + 1) % arraySize;
	}
}


template <typename T>
void
ArrayList<T>::delBackPos() {
	if (!empty()) {
		backPos = (backPos - 1) % arraySize;
	}
}


template <typename T>
bool
ArrayList<T>::needsResized() const {
	return frontPos == backPos;
}
