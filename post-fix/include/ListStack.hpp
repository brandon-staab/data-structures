/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include "ArrayList.hpp"
#include "Stack.hpp"


template <typename T>
class ListStack : public Stack<T> {
	using value_type = typename Stack<T>::value_type;
	using reference = typename Stack<T>::reference;
	using const_reference = typename Stack<T>::const_reference;

  public:
	ListStack() = default;
	ListStack(ListStack const& other) = default;
	ListStack& operator=(ListStack const& other) = default;

	~ListStack() = default;

	/*** Member functions *****************************************************/
	bool empty() const;
	int size() const;
	reference top();
	const_reference top() const;
	void push(const_reference val);
	void push(value_type&& val);
	void pop();

  private:
	ArrayList<T> stack;
};


#include "ListStack.tpp"
