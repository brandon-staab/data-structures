/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

template <typename T>
struct Token {
	char name;
	T value;
};

template <typename T>
std::ostream&
operator<<(std::ostream& os, Token<T> const& t) {
	os << t.name << ' ' << t.value << '\t';
	return os;
}
