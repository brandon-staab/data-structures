/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include "ArrayList.hpp"
#include "Token.hpp"

#include <iosfwd>


template <typename T>
struct Postfix {
	ArrayList<char> output;
	ArrayList<Token<T>> tokens;
};

template <typename T>
std::ostream& operator<<(std::ostream& os, Postfix<T> const& expr);


#include "Postfix.tpp"
