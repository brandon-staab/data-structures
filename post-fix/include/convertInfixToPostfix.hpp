/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include "Postfix.hpp"

#include <string>


template <typename T>
Postfix<T> convertInfixToPostfix(std::string const& input);

const std::size_t getPrecedence(char const op);


#include "convertInfixToPostfix.tpp"
