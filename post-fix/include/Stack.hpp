/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include <cstddef>


template <typename T>
class Stack {
  public:
	using value_type = T;
	using reference = value_type&;
	using const_reference = const reference;

  public:
	/*** Member functions *****************************************************/
	virtual bool empty() const = 0;
	virtual int size() const = 0;
	virtual reference top() = 0;
	virtual const_reference top() const = 0;
	virtual void push(const_reference val) = 0;
	virtual void push(value_type&& val) = 0;
	virtual void pop() = 0;
};
