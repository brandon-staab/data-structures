/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */
#pragma once

#include "List.hpp"

#include <cstdint>
#include <memory>


template <typename T>
class ArrayList : public List<T> {
	using value_type = typename List<T>::value_type;
	using reference = typename List<T>::reference;
	using const_reference = typename List<T>::const_reference;
	using pointer = typename List<T>::pointer;
	using const_pointer = typename List<T>::const_pointer;
	using iterator = typename List<T>::iterator;
	using const_iterator = typename List<T>::const_iterator;
	using size_type = typename List<T>::size_type;
	using pos_type = std::uintmax_t;

	static const typename List<T>::size_type DEFAULT_SIZE = 32;
	static const typename List<T>::size_type NULL_POS = 0;

  public:
	ArrayList(size_type size = DEFAULT_SIZE);
	ArrayList(const_reference other);

	void operator=(reference other);


	/*** Capacity *************************************************************/
	bool empty() const noexcept;
	size_type size() const noexcept;
	size_type max_size() const noexcept;

	/*** Element access *******************************************************/
	reference front();
	const_reference front() const;
	reference back();
	const_reference back() const;

	/*** Modifiers ************************************************************/
	void push_front(const_reference val);
	void push_front(value_type&& val);
	void pop_front();
	void push_back(const_reference val);
	void push_back(value_type&& val);
	void pop_back();
	void resize(size_type n);

	/*** Cursor ***************************************************************/
	void setBegin() const;
	void setEnd() const;
	bool isBegin() const;
	bool isEnd() const;
	void next() const;
	void prev() const;
	reference getItem();
	const_reference getItem() const;

  private:
	pos_type frontPos;
	pos_type backPos;
	mutable pos_type cursor;
	size_type arraySize;
	std::unique_ptr<value_type[]> array;

	bool addFrontPos();
	bool addBackPos();
	void delFrontPos();
	void delBackPos();
	bool needsResized() const;
};


#include "ArrayList.tpp"
