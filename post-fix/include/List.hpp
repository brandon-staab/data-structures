/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include <cstddef>


template <typename T>
class List {
  public:
	using value_type = T;
	using reference = value_type&;
	using const_reference = const reference;
	using pointer = value_type*;
	using const_pointer = const pointer;
	using iterator = T*;
	using const_iterator = const iterator;
	using size_type = std::size_t;

  public:
	/*** Capacity *************************************************************/
	virtual bool empty() const noexcept = 0;
	virtual size_type size() const noexcept = 0;
	virtual size_type max_size() const noexcept = 0;

	/*** Element access *******************************************************/
	virtual reference front() = 0;
	virtual const_reference front() const = 0;
	virtual reference back() = 0;
	virtual const_reference back() const = 0;

	/*** Modifiers ************************************************************/
	virtual void push_front(const_reference val) = 0;
	virtual void push_front(value_type&& val) = 0;
	virtual void pop_front() = 0;
	virtual void push_back(const_reference val) = 0;
	virtual void push_back(value_type&& val) = 0;
	virtual void pop_back() = 0;
	virtual void resize(size_type n) = 0;
};
