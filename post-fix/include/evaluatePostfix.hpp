/**
 *  \brief		Project 1: Postfix
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.1.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include "Postfix.hpp"


template <typename T>
T evaluatePostfix(Postfix<T>& expr);

const bool isBinaryOp(char const c);


#include "evaluatePostfix.tpp"
