/**
 *  \brief		Project 2: Max Heap
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */
#pragma once

#include <iosfwd>


struct Student {
	char first[21];
	char last[21];
	int id;
	float GPA;
};


bool cmp_id_greater(Student const& a, Student const& b);
bool cmp_last_greater(Student const& a, Student const& b);

std::ofstream& operator<<(std::ofstream& of, Student& student);
std::ifstream& operator>>(std::ifstream& inf, Student& student);

std::ostream& operator<<(std::ostream& os, Student const& student);
std::istream& operator>>(std::istream& is, Student& student);
