/**
 *  \brief		Project 2: Max Heap
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */
#pragma once

#include "Student.hpp"

#include <iosfwd>
#include <vector>


void print(std::ostream& os, std::vector<Student> const& students);

std::ostream& operator<<(std::ostream& os, std::vector<Student> const& students);
