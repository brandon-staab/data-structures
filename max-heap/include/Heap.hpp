/**
 *  \brief		Project 2: Max Heap
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#pragma once

#include <memory>
#include <stack>
#include <vector>


template <typename T>
class Heap {
  public:
	using CMP_FN = bool (*)(T const&, T const&);

	Heap();
	Heap(CMP_FN cmp_fn, std::size_t capacity = 32);
	Heap(Heap const& other);

	Heap& operator=(const Heap& other);
	T& operator[](std::size_t const index);
	const T& operator[](std::size_t const index) const;

	bool empty() const;
	std::size_t size() const;
	std::size_t capacity() const;

	T& top();
	T const& top() const;

	void insert(T const& val);
	void insert(T&& val);

	void merge(Heap& other);

	void pop();
	void remove(std::size_t const index);
	void clear();

	std::vector<T> ascending() const;
	std::vector<T> descending() const;

	CMP_FN getCompareFN() const;

  private:
	std::size_t num_of_items;
	std::size_t max_capacity;
	std::unique_ptr<T[]> data;
	CMP_FN cmp_fn;

	bool canInsert();
	void heapify(std::size_t const index);
	void resize(std::size_t const new_size);
};


#include "Heap.tpp"
