/**
 *  \brief		Project 2: Max Heap
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include <algorithm>
#include <cassert>


inline std::size_t
double_if_full(std::size_t const num_of_items, std::size_t const max_capacity) {
	bool is_full = (4 * (num_of_items - max_capacity)) / max_capacity;
	return is_full ? (max_capacity) : (2 * max_capacity);
}


template <typename T>
Heap<T>::Heap() : num_of_items(0), max_capacity(0), data(), cmp_fn(){};


template <typename T>
Heap<T>::Heap(CMP_FN cmp_fn, std::size_t capacity) : num_of_items(0), max_capacity(capacity), data(new T[capacity]), cmp_fn(cmp_fn){};


template <typename T>
Heap<T>::Heap(Heap const& other) :
	num_of_items(other.num_of_items),
	max_capacity(double_if_full(other.num_of_items, other.max_capacity)),
	data(new T[max_capacity]),
	cmp_fn(other.cmp_fn) {
	std::copy_n(other.data.get(), other.size(), data.get());
}


template <typename T>
Heap<T>&
Heap<T>::operator=(Heap<T> const& other) {
	if (this != &other) {
		if (other.size() > capacity()) {
			max_capacity = double_if_full(other.num_of_items, other.max_capacity);
			data.reset(new T[max_capacity]);
		}

		num_of_items = other.num_of_items;
		cmp_fn = other.cmp_fn;

		assert(other.size() <= capacity());
		std::copy_n(other.data.get(), other.size(), data.get());
	}

	return *this;
}


template <typename T>
T& Heap<T>::operator[](std::size_t const index) {
	assert(index < size());

	return data[index];
}


template <typename T>
T const& Heap<T>::operator[](std::size_t const index) const {
	assert(index < size());

	return data[index];
}


template <typename T>
bool
Heap<T>::empty() const {
	return size() == 0;
}


template <typename T>
std::size_t
Heap<T>::size() const {
	return num_of_items;
}


template <typename T>
std::size_t
Heap<T>::capacity() const {
	return max_capacity;
}


template <typename T>
T&
Heap<T>::top() {
	return data[0];
}


template <typename T>
T const&
Heap<T>::top() const {
	return data[0];
}


template <typename T>
void
Heap<T>::insert(const T& val) {
	if (!canInsert()) {
		resize(2 * capacity());
	}

	std::size_t index = num_of_items++;
	data[index] = val;

	while (cmp_fn(data[index], data[index / 2])) {
		std::swap(data[index], data[index / 2]);
		index /= 2;
	}
}


template <typename T>
void
Heap<T>::insert(T&& val) {
	if (!canInsert()) {
		resize(2 * capacity());
	}

	std::size_t index = num_of_items++;
	data[index] = val;

	while (cmp_fn(data[index], data[index / 2])) {
		std::swap(data[index], data[index / 2]);
		index /= 2;
	}
}


template <typename T>
void
Heap<T>::merge(Heap& other) {
	while (!other.empty()) {
		insert(other.top());
		other.pop();
	}
}


template <typename T>
void
Heap<T>::pop() {
	remove(0);
}


template <typename T>
void
Heap<T>::clear() {
	num_of_items = 0;
}


template <typename T>
void
Heap<T>::remove(std::size_t const index) {
	assert(size() != 0);
	assert(index < size());

	if (size() > 1) {
		std::swap(data[index], data[--num_of_items]);
		heapify(index);
	} else if (size() == 1) {
		--num_of_items;
	}
}


template <typename T>
std::vector<T>
Heap<T>::ascending() const {
	auto desc = std::stack<T, std::vector<T>>(descending());
	std::vector<T> asc;

	while (!desc.empty()) {
		asc.push_back(desc.top());
		desc.pop();
	}

	return asc;
}


template <typename T>
std::vector<T>
Heap<T>::descending() const {
	auto copy = Heap<T>(*this);
	std::vector<T> desc;

	while (!copy.empty()) {
		desc.push_back(copy.top());
		copy.pop();
	}

	return desc;
}


template <typename T>
typename Heap<T>::CMP_FN
Heap<T>::getCompareFN() const {
	return cmp_fn;
}


template <typename T>
bool
Heap<T>::canInsert() {
	return size() + 1 <= capacity();
}


template <typename T>
void
Heap<T>::heapify(std::size_t const index) {
	assert(index < size());

	std::size_t left = 2 * index;
	std::size_t right = 2 * index + 1;
	std::size_t largest = index;

	if (left < size() && cmp_fn(data[left], data[largest])) {
		largest = left;
	}

	if (right < size() && cmp_fn(data[right], data[largest])) {
		largest = right;
	}

	if (largest != index) {
		std::swap(data[index], data[largest]);
		heapify(largest);
	}
}


template <typename T>
void
Heap<T>::resize(std::size_t const new_size) {
	T* old_data = data.release();
	max_capacity = std::max({8ul, new_size, max_capacity});
	data.reset(new T[max_capacity]);

	assert(capacity() > size());
	std::copy_n(old_data, size(), data.get());

	delete[] old_data;
}
