/**
 *  \brief		Project 2: Max Heap
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Student.hpp"

#include <cstring>
#include <fstream>
#include <iostream>


bool
cmp_id_greater(const Student& a, Student const& b) {
	return a.id > b.id;
}


bool
cmp_last_greater(const Student& a, Student const& b) {
	return std::strcmp(a.last, b.last) > 0;
}


std::ofstream&
operator<<(std::ofstream& of, Student& student) {
	of.write(student.first, sizeof(Student::first));
	of.write(student.last, sizeof(Student::last));
	of.write(reinterpret_cast<char*>(&student.id), sizeof(Student::id));
	of.write(reinterpret_cast<char*>(&student.GPA), sizeof(Student::GPA));

	return of;
}


std::ifstream&
operator>>(std::ifstream& inf, Student& student) {
	inf.read(student.first, sizeof(Student::first));
	if (std::strlen(student.first) > 20) {
		inf.setstate(std::ios::failbit);
		return inf;
	}

	inf.read(student.last, sizeof(Student::last));
	if (std::strlen(student.last) > 20) {
		inf.setstate(std::ios::failbit);
		return inf;
	}

	inf.read(reinterpret_cast<char*>(&student.id), sizeof(Student::id));
	inf.read(reinterpret_cast<char*>(&student.GPA), sizeof(Student::GPA));
	if (!(-1.0 <= student.GPA && student.GPA <= 5.0)) {
		inf.setstate(std::ios::failbit);
		return inf;
	}


	return inf;
}


std::ostream&
operator<<(std::ostream& os, Student const& student) {
	return os << student.first << ' ' << student.last << ' ' << student.id << ' ' << student.GPA;
}


std::istream&
operator>>(std::istream& is, Student& student) {
	is >> student.first;
	if (std::strlen(student.first) > 20) {
		is.setstate(std::ios::failbit);
		return is;
	}

	is >> student.last;
	if (std::strlen(student.last) > 20) {
		is.setstate(std::ios::failbit);
		return is;
	}

	is >> student.id;

	is >> student.GPA;
	if (!(-1.0 <= student.GPA && student.GPA <= 5.0)) {
		is.setstate(std::ios::failbit);
		return is;
	}

	return is;
}
