/**
 *  \brief		Project 2: Max Heap
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "printStudentsVec.hpp"

#include <iomanip>
#include <iostream>
#include <vector>


void
print(std::ostream& os, std::vector<Student> const& students) {
	os << R"(/--------------------|--------------------|------|-------\
|       First        |        Last        |  ID  |  GPA  |
|--------------------|--------------------|------|-------|
)";

	for (auto student : students) {
		os << '|' << std::setw(20);
		os << student.first;

		os << '|' << std::setw(20);
		os << student.last;

		os << '|' << std::setw(6);
		os << student.id;

		os << '|' << std::setw(7) << std::setprecision(5) << std::fixed;
		os << student.GPA << "|\n";
	}

	os << "\\--------------------|--------------------|------|-------/\n";
}


std::ostream&
operator<<(std::ostream& os, std::vector<Student> const& students) {
	print(os, students);

	return os;
}
