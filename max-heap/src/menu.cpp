/**
 *  \brief		Project 2: Max Heap
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "menu.hpp"

#include "getConsoleInput.hpp"

#include <iostream>


int
menu() {
	std::cout << R"((1) Read data from external text file.
(2) Build a max heap in terms of ID or LastName.
(3) Add a new record to max heap.
(4) Delete a record from the max heap.
(5) Print sorted list from max heap in ascending order based on ID or LastName.
(6) Save a sorted list to external binary file named as 316p2.rec
(7) Load the binary file and print the list by one record per line.
(8) Quit.
)";

getInput:
	int selection = getConsoleInput<int>();

	if (selection < 1 || selection > 8) {
		std::cout << "Valid options are between 1 and 8.\n";
		goto getInput;
	}

	return selection;
}
