/**
 *  \brief		Project 2: Max Heap
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "Heap.hpp"
#include "Student.hpp"
#include "getConsoleInput.hpp"
#include "menu.hpp"
#include "printStudentsVec.hpp"

#include <cstring>
#include <fstream>
#include <iostream>
#include <sstream>
#include <vector>


std::string const BINARY_FILE = "316p2.rec";
std::string const MSG_ERR_OPENFILE = "Unable to open file.\n";


int
main() {
	bool running_flag = true;
	bool heap_initialized = false;
	int choice;
	Heap<Student> students;

	do {
		choice = menu();

		if (!heap_initialized && !(choice == 2 || choice == 8)) {
			std::cerr << "Heap must be built first.\n";
			continue;
		}

		if (students.empty() && (choice == 4 || choice == 5 || choice == 6)) {
			std::cout << "The heap is empty.\n";
			continue;
		}

		switch (choice) {
			case 1: {
				static std::string filename = "cs316p2.dat";
				std::string line;
				Student student;

				std::cout << "Load contents of \"" << filename << "\" into the max heap? [Y/N]\n";
			get_filename:
				char response = std::tolower(getConsoleInput<char>());
				if (response == 'y') {
					// Do nothing
				} else if (response == 'n') {
					std::cout << "Enter the name of the input file with the file extention.\n";
					filename = getConsoleInput<std::string>();
				} else {
					std::cerr << "Valid choices are [Y/N]\n";
					goto get_filename;
				}

				std::ifstream file(filename.c_str());
				if (!file.is_open()) {
					std::cerr << MSG_ERR_OPENFILE;
					break;
				}

				while (getline(file, line)) {
					if (!line.empty()) {
						if ((static_cast<std::stringstream>(line) >> student).fail()) {
							std::cerr << "Could not add \"" << line << "\" to the heap.\n";
						} else {
							students.insert(student);
						}
					}
				}

				file.close();
			} break;

			case 2: {
				std::cout << "Is the max heap sorted by ID? ...Otherwise LastName [Y/N]\n";
			get_heap_type:
				char response = std::tolower(getConsoleInput<char>());

				Heap<Student>::CMP_FN cmp_fn;
				if (response == 'y') {
					cmp_fn = cmp_id_greater;
				} else if (response == 'n') {
					cmp_fn = cmp_last_greater;
				} else {
					std::cerr << "Valid choices are [Y/N]\n";
					goto get_heap_type;
				}

				if (!heap_initialized) {
					students = Heap<Student>(cmp_fn);
					heap_initialized = true;
				} else if (students.getCompareFN() != cmp_fn) {
					// backup the heap
					auto backup_copy = Heap<Student>(students);

					// create a new heap
					students = Heap<Student>(cmp_fn);

					// merge the backup into the new heap
					students.merge(backup_copy);
				}
			} break;

			case 3: {
				std::cout << R"(Enter a new student:
FirstName   LastName   ID   GPA
-------------------------------
)";
				students.insert(getConsoleInput<Student>());
			} break;

			case 4: {
				auto index = -1;

				std::cout << R"((1) Delete by ID
(2) Delete by LastName
)";
			get_del_mode:
				auto del_mode = getConsoleInput<int>();
				if (del_mode < 1 || 2 < del_mode) {
					std::cout << "Valid options are between 1 and 2.\n";
					goto get_del_mode;
				}

				if (del_mode == 1) {
					std::cout << "Enter an ID to delete.\n";

					auto id = getConsoleInput<int>();

					for (auto i = 0u; i < students.size(); i++) {
						if (students[i].id == id) {
							index = i;
							break;
						}
					}
				} else {
					std::cout << "Enter a LastName to delete\n";

				get_last:
					auto last = getConsoleInput<std::string>();
					if (last.empty() || 20 < last.length()) {
						std::cout << "Length must be between 1 and 20\n";
						goto get_last;
					}

					for (auto i = 0u; i < students.size(); i++) {
						if (students[i].last == last) {
							index = i;
							break;
						}
					}
				}

				if (index == -1) {
					std::cout << "Student not found.\n";
					break;
				}

				students.remove(index);
			} break;

			case 5: {
				std::cout << students.ascending();
			} break;

			case 6: {
				auto file = std::ofstream(BINARY_FILE.c_str(), std::ios::out | std::ios::binary | std::ios::trunc);
				if (!file.is_open()) {
					std::cerr << MSG_ERR_OPENFILE;
					break;
				}

				for (auto student : students.descending()) {
					file << student;
				}
			} break;

			case 7: {
				Student student;
				std::streampos file_size;

				auto file = std::ifstream(BINARY_FILE.c_str(), std::ios::in | std::ios::binary | std::ios::ate);
				if (!file.is_open()) {
					std::cerr << MSG_ERR_OPENFILE;
					break;
				}

				file_size = file.tellg();
				file.seekg(0, std::ios::beg);

				if (file_size == 0) {
					std::cout << BINARY_FILE << " is empty.\n";
					break;
				}

				while (file.good()) {
					if (!(file >> student).fail()) {
						std::cout << "Inserting \"" << student << "\"\n";
						students.insert(student);
					}
				}

				if (file.fail() && !file.eof()) {
					std::cerr << "Fatal error loading " << BINARY_FILE << ".  Clearing contents of max heap.\n";
					students.clear();
					file.clear();
				}
			} break;

			case 8: {
				running_flag = false;
			} break;
		}
	} while (running_flag);
}
