/**
 *  \brief		Project 2: Max Heap
 *  \details	Data Structures project for Dr. C.-C. Chan
 *  \author		Brandon Staab <bls114@zips.uakron.edu>
 *  \version	1.0.0
 *  \date		October 2017
 *  \copyright	COPYRIGHT (C) 2017 Brandon Staab (bls114) All rights reserved.
 */

#include "getConsoleInput.hpp"

#include "Student.hpp"

#include <iostream>
#include <limits>
#include <string>
#include <type_traits>


/*** Explicit instantiation definition  ***************************************/
template bool getConsoleInput<bool>();
template char getConsoleInput<char>();
template int getConsoleInput<int>();
template std::size_t getConsoleInput<std::size_t>();
template std::string getConsoleInput<std::string>();

template Student getConsoleInput<Student>();


namespace {
	template <typename T>
	constexpr std::string
	getType(T) {
		if constexpr (std::is_same<T, bool>::value) {
			return "bit";
		}

		if constexpr (std::is_same<T, char>::value) {
			return "letter";
		}

		if constexpr (std::is_signed<T>::value) {
			return "number";
		}

		if constexpr (std::is_unsigned<T>::value) {
			return "positive number";
		}

		if constexpr (std::is_floating_point<T>::value) {
			return "floating point number";
		}

		if constexpr (std::is_same<T, std::string>::value) {
			return "string";
		}

		return "data stream";
	}
}  // namespace


template <typename T>
T
getConsoleInput() {
	T value;

	if (!std::cin) {
		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max());
	}

	while (true) {
		std::cout << "> ";

		if constexpr (std::is_same<T, std::string>::value) {
			std::getline(std::cin, value);
			if (std::cin && !value.empty()) {
				return value;
			}
		} else {
			std::cin >> value;
			if (std::cin) {
				return value;
			}
		}

		std::cin.clear();
		std::cin.ignore(std::numeric_limits<std::streamsize>::max(), '\n');

		std::cerr << "Error: bad input\n";
		std::cout << "Enter a " << getType(value) << ":" << std::endl;
	}
}
